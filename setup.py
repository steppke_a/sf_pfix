from setuptools import setup, find_packages

setup(
    name='sfpfix',
    version='0.1',
    packages=['sfpfix'],
    package_dir={'': 'src'},
    install_requires=['joblib', 'sfdata', 'loguru'],
    author='Alexander Steppke',
    author_email='alexander.steppke@psi.ch',
    description='Parallel processing of SwissFEL data using SFDataFiles',
    url='https://gitlab.psi.ch/steppke_a/sf_pfix',
)