from sfdata import SFDataFiles, SFScanInfo
from loguru import logger
import numpy as np


def cache_if_available(func):
    """Decorator to use the joblib cache for a function
    if we can obtain import the required module is available."""
    try:
        from cristallina import analysis

        return analysis.memory.cache(func)
    except Exception as e:
        logger.info(f"Cannot import cristallina.analysis. Disabling cache: {e}")
        return func


@cache_if_available
def process_single_step_example(index, step_files, channels, preview=False, **kwargs):
    """To process a single step in general we require the corresponding files and channels."""
    with SFDataFiles(*step_files) as sfdata:
        subset = sfdata[channels]
        subset.drop_missing()

        sums = np.sum(subset["JF17T16V01"][:], axis=(1, 2))
        pids = subset.pids.copy()

    return {"pids": pids, "JF_SUM": sums}


def process_image_example(single_pulse_data, delay=0.1):
    """Processing example which takes at least <delay> seconds to complete.
    Later will be fitting, correlation, windowing,...
    """
    y = delay * 100000000 / 4  # very roughly calibrated for seconds
    x = 0
    while x < y:
        x += 1

    return np.sum(single_pulse_data)


def process_channels_batch_example(pids, ch_names, chs_batches):
    """Example for processing a batch of channel data.

    Parameters:
    pids: pulse ids of this batch
    chs_batches: list of batches from individual channels

    """
    channel_num = 0
    return [process_image_example(image, delay=0.1) for image in chs_batches[channel_num]]
