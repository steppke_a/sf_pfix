import os
import itertools
import numpy as np

from joblib import Parallel, delayed

from sfdata import SFDataFiles, SFScanInfo

from loguru import logger


def process_scan_steps(
    scan: SFScanInfo,
    process_step_fun: callable,
    channels,
    parallelize: bool = True,
    max_cpus: int = 14,
    verbose: int = 0,
    **process_kwargs,
) -> list:
    """Processes all of the steps in a scan in parallel. Each step is processed individually
    by process_step_fun for the given list of channels.

    Parameters:
    scan: The scan that contains the steps to process.
    process_step_fun: The function to process each step.
    parallelize: Whether to parallelize the processing. Default is True.
    max_cpus: The maximum number of CPUs to use for parallelization. Default is 14.
              Be aware that each job also requires enough memory and will get killed otherwise.
    verbose: The verbosity level of the parallel processing. Default is 0.

    """

    n_jobs = calc_num_cpus(len(scan), max_cpus)
    backend = "loky"  # best choice for most situations

    if parallelize:
        parallel_processor = Parallel(n_jobs=n_jobs, backend=backend, verbose=verbose)
        chained_results = parallel_processor(
            delayed(process_step_fun)(i, step.fnames, channels, **process_kwargs)
            for i, step in enumerate(scan)
        )
    else:
        chained_results = [
            process_step_fun(i, step.fnames, channels, **process_kwargs) for i, step in enumerate(scan)
        ]

    return chained_results


def process_parallel_batches(
    files,
    channels,
    process_channels_batch_func: callable,
    batch_size=20,
    preview=False,
    drop_missing=True,
    parallelize=True,
    n_jobs=10,
    verbose=0,
    backend="loky",
):
    """This function processes batches of channel data in parallel.
    It requires the files to be processed, the channels to be processed and the function to process each batch.

    By default drops missing pulses in the intersection of the channels.

    It returns the pulse IDs and the results of processing each batch.
    """
    with SFDataFiles(*files) as sfdata:
        subset = sfdata[channels]
        if drop_missing:
            subset.drop_missing()

        chs = [sfdata[channel] for channel in channels]
        ch_names = [c.name for c in chs]
        all_pids = chs[0].pids

        def parallel_helper(ch_names, chs_batch):
            """Takes apart a combined batch of channels to extract
            pulseids and channel batches.
            Feeds the pulseids and channel batches to further processing.
            """
            c_batches = [c_batch for indices, c_batch in chs_batch]
            batch_indices = [indices for indices, c_batch in chs_batch]
            batch_pids = all_pids[batch_indices[0]]

            return process_channels_batch_func(batch_pids, ch_names, c_batches)

        if parallelize:
            parallel_processor = Parallel(n_jobs=n_jobs, backend=backend, verbose=verbose)

            # Beware this consumes approximately 2*size of data of RAM:
            # ... for chs_batch in list(zip(*[c.in_batches(size=batch_size) for c in chs])))

            # default batch size: consumes approximately 1/2 * size of data of RAM
            # only zipping the batches with smaller batch size consumes approximately 1/5 * size of data of RAM:
            chained_results = parallel_processor(
                delayed(parallel_helper)(ch_names, chs_batch)
                for chs_batch in zip(*[c.in_batches(size=batch_size) for c in chs])
            )
        else:
            chained_results = []
            for chs_batch in zip(*[c.in_batches(size=batch_size) for c in chs]):
                c_batches = [c_batch for indices, c_batch in chs_batch]
                batch_indices = [indices for indices, c_batch in chs_batch]
                batch_pids = all_pids[batch_indices[0]]

                chained_results.append(process_channels_batch_func(batch_pids, ch_names, c_batches))

    # flatten results accordingly
    results = np.array(list(itertools.chain.from_iterable(chained_results)))

    # Postcondition: we assume that for each image we obtain one processed object
    assert len(results) == len(all_pids), "Pulse IDs do not match processed data."

    return all_pids, results


def calc_num_cpus(num_steps: int, max_cpus: int = 14) -> int:
    """Estimate number of CPUs to use for parallelization
    depending on reservation of a SLURM job.
    """
    try:
        cpus_per_node = max(1, int(os.environ["SLURM_JOB_CPUS_PER_NODE"]))
        cpus_per_task = max(1, int(os.environ["SLURM_CPUS_PER_TASK"]))

        # leave one allocated CPU free, otherwise not more than number of steps
        num_jobs = min([cpus_per_node - 1, cpus_per_task - 1, num_steps, max_cpus])
        # but at least one
        num_jobs = max(num_jobs, 1)

    except (KeyError, ValueError):
        num_jobs = min([num_steps, max_cpus])

    return num_jobs
