[![pipeline status](https://gitlab.psi.ch/steppke_a/sf_pfix/badges/main/pipeline.svg)](https://gitlab.psi.ch/steppke_a/sf_pfix/-/commits/main)
[![Latest Release](https://gitlab.psi.ch/steppke_a/sf_pfix/-/badges/release.svg)](https://gitlab.psi.ch/steppke_a/sf_pfix/-/releases)
[![coverage report](https://gitlab.psi.ch/steppke_a/sf_pfix/badges/main/coverage.svg)](https://gitlab.psi.ch/steppke_a/sf_pfix/-/commits/main)
  

# sf_pfix

Simple parallel processing of SwissFEL data. 

As many processing tasks for X-ray data are not coupled we can process them in parallel. This package helps to facilitate the parallelization with a few helper functions based on [joblib](https://github.com/joblib/joblib) and [SFData][link1].

## Parallelize over scan steps
A quick example how to compute parameters of a step within a larger scan in parallel. We define a processing function that operates on a single step:


````python
import sfpfix
import numpy as np

from sfdata import SFScanInfo, SFDataFiles

def process_func(index, step_files, channels,  **kwargs):
   
    with SFDataFiles(*step_files) as sfdata:
        subset = sfdata[channels]
        subset.drop_missing()

        sums = np.sum(subset["JF17T16V01"][:], axis=(1, 2))
        pids = subset.pids.copy()

    return {"pids": pids, "JF_SUM": sums}
````

and load a scan and process the individual steps in parallel:

````python
scan = SFScanInfo("/sf/cristallina/data/p21741/raw/run1200/meta/scan.json")
channels = ["JF17T16V01", "SARFE10-PSSS059:SPECTRUM_Y_SUM"]

results = sfpfix.parallel.process_scan_steps(scan, process_func, channels)
````

Here results is a list where each entry (step) is a dict from `process_func`.

If the number of images per step is sufficiently large (e.g. > 50) the runtime scales almost as 1/n as very little data needs to be passed around and the process startup time is negligible. 

## Parallelize over batches
Alternatively we can also parallelize the processing of a single acquisition with many pulses by splitting up the dataset in batches and process them individually.

As an example the individual images are processed by
````python
def process_image(single_pulse_data):
    return np.sum(single_pulse_data)
    
````

And then with a small helper we can parallelize over batches:

````python
def process_channels_batch(pids, ch_names, chs_batches):
    channel_num = 0
    return [process_image(image) for image in chs_batches[channel_num]]


scan = SFScanInfo("/sf/cristallina/data/p21741/raw/run1200/meta/scan.json")

channels = ["JF17T16V01", "SARFE10-PSSS059:SPECTRUM_Y_SUM"]

pids, sums = sfpfix.parallel.process_parallel_batches(scan[0].fnames, channels, process_channels_batch)
````

Here we need to keep in mind that spawning processes is a relatively expensive operation and additionally a lot of data is passed between the processes. This means that parallelization over batches is only useful for longer running processing functions. In an example benchmark on RA the cutoff was between 1 and 10 ms per image:


![image](fig_benchmark_batches.png)

## Installation

`git clone` this repository and then install the development version using `pip install -e .`

Requires at minimum somewhat recent versions of numpy, joblib and sfdata. 

[link1]: https://github.com/paulscherrerinstitute/sf_datafiles