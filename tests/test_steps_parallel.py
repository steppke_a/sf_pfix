import pytest

import warnings

import numpy as np


from sfdata import SFScanInfo, SFDataFiles
import sfpfix

def process_func(index, step_files, channels,  **kwargs):
    """ Example process function over a single scan step. """
    
    with SFDataFiles(*step_files) as sfdata:
        subset = sfdata[channels]
        subset.drop_missing()

        jf_sums = np.sum(subset["JF16T03V01"][:], axis=(1, 2))
        gas_monitor = subset["SARFE10-PBIG050-EVR0:CALCI"].data
        
        pids = subset.pids

    return {"PIDS": pids, "JF_SUM": jf_sums, "GAS_MONITOR": gas_monitor}

@pytest.mark.filterwarnings("ignore::UserWarning")
def test_parallel_summation():
    scan = SFScanInfo("tests/data/run0376/meta/scan.json")
    results = sfpfix.parallel.process_scan_steps(scan, process_func, ["JF16T03V01", "SARFE10-PBIG050-EVR0:CALCI"])
    
    assert len(results) == 6, "not all steps processed"
    assert np.allclose([-11817.599 ,  -6957.8   ,  -8025.4004], results[0]["JF_SUM"])