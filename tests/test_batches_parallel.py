import pytest
import warnings

import numpy as np

from sfdata import SFScanInfo, SFDataFiles
import sfpfix

def process_image(single_pulse_data):
    return np.sum(single_pulse_data)
    
def process_channels_batch(pids, ch_names, chs_batches):
    channel_num = 0
    return [process_image(image) for image in chs_batches[channel_num]]


@pytest.mark.filterwarnings("ignore::UserWarning")
def test_parallel_summation():
    scan = SFScanInfo("tests/data/run0376/meta/scan.json")
    channels = ["JF16T03V01", "SARFE10-PBIG050-EVR0:CALCI"]

    pids, sums = sfpfix.parallel.process_parallel_batches(scan[0].fnames, channels, process_channels_batch)
    assert len(sums) == 3, "not all shots processed"

    assert np.allclose([-11817.599 ,  -6957.8   ,  -8025.4004], sums)

